"""
Video + Music Stream Telegram Bot
Copyright (c) 2022-present levina=lab <https://github.com/levina-lab>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but without any warranty; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/licenses.html>
"""


from driver.core import me_bot, me_user
from driver.queues import QUEUE
from driver.decorators import check_blacklist
from program.utils.inline import menu_markup, stream_markup

from pyrogram import Client, filters
from pyrogram.types import CallbackQuery, InlineKeyboardButton, InlineKeyboardMarkup

from config import (
    BOT_USERNAME,
    GROUP_SUPPORT,
    OWNER_USERNAME,
    UPDATES_CHANNEL,
    SUDO_USERS,
    OWNER_ID,
)


@Client.on_callback_query(filters.regex("home_start"))
@check_blacklist()
async def start_set(_, query: CallbackQuery):
    await query.answer("主页面")
    await query.edit_message_text(
        f"""✨ **欢迎 [{query.message.chat.first_name}](tg://user?id={query.message.chat.id}) !**\n
💭 [{me_bot.first_name}](https://t.me/{BOT_USERNAME}) **可以让你通过 Telegram 的视频聊天功能在群组中播放音乐和视频！**

💡 **要了解机器人的所有命令，请点击 » 📚 命令**

🔖 **要知道如何使用这个机器人，请点击 » ❓ 基础教程**""",
        reply_markup=InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton(
                        "➕ 邀请我到你的群组 ➕",
                        url=f"https://t.me/{BOT_USERNAME}?startgroup=true",
                    )
                ],
                [InlineKeyboardButton("❓ 基础教程", callback_data="user_guide")],
                [
                    InlineKeyboardButton("📚 命令", callback_data="command_list"),
                    InlineKeyboardButton("❤ 捐赠", url=f"https://t.me/{OWNER_USERNAME}"),
                ],
                [
                    InlineKeyboardButton(
                        "👥 官方群组", url=f"https://t.me/{GROUP_SUPPORT}"
                    ),
                    InlineKeyboardButton(
                        "📣 官方频道", url=f"https://t.me/{UPDATES_CHANNEL}"
                    ),
                ],
                [
                    InlineKeyboardButton(
                        "🌐 源代码", url="https://gitlab.com/Xtao-Labs/video-stream"
                    )
                ],
            ]
        ),
        disable_web_page_preview=True,
    )


@Client.on_callback_query(filters.regex("quick_use"))
@check_blacklist()
async def quick_set(_, query: CallbackQuery):
    await query.answer("快速使用指南")
    await query.edit_message_text(
        f"""ℹ️ 快速使用指南

👩🏻‍💼 » /play - 使用 歌名 或者 youtube 链接 或者 回复音乐 来播放

👩🏻‍💼 » /vplay - 使用 歌名 或者 youtube 链接 或者 回复视频 来播放

👩🏻‍💼 » /vstream - 使用 YouTube 直播链接 或者 m3u8 链接 来播放

❓ 有疑问？请加入 [支持群组](https://t.me/{GROUP_SUPPORT}).""",
        reply_markup=InlineKeyboardMarkup(
            [[InlineKeyboardButton("🔙 返回", callback_data="user_guide")]]
        ),
        disable_web_page_preview=True,
    )


@Client.on_callback_query(filters.regex("user_guide"))
@check_blacklist()
async def guide_set(_, query: CallbackQuery):
    await query.answer("基础教程")
    await query.edit_message_text(
        f"""❓ 如何使用此机器人？

1.) 首先添加我到你的群组
2.) 然后给我管理员权限
3.) 使用 /reload 重载管理员列表
3.) 邀请 @{me_user.username} 到群组或者使用 /userbotjoin 
4.) 开始使用

`- 最后，一切都已经设置好了 -`

📌 如果机器人没有正常工作，请确认视频聊天是否已经打开，userbot 是否在群组中。

💡 有疑问？请加入 @{GROUP_SUPPORT}.""",
        reply_markup=InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton("» 快速使用指南 «", callback_data="quick_use")
                ],[
                    InlineKeyboardButton("🔙 返回", callback_data="home_start")
                ],
            ]
        ),
    )


@Client.on_callback_query(filters.regex("command_list"))
@check_blacklist()
async def commands_set(_, query: CallbackQuery):
    user_id = query.from_user.id
    await query.answer("命令目录")
    await query.edit_message_text(
        f"""✨ **你好 [{query.message.chat.first_name}](tg://user?id={query.message.chat.id}) ！**

» 请选择下面的子菜单来查看可用的命令列表!

你可以使用三个命令前缀：(`! / .`)""",
        reply_markup=InlineKeyboardMarkup(
            [
                [
                    InlineKeyboardButton("👮🏻‍♀️ 管理员命令", callback_data="admin_command"),
                ],[
                    InlineKeyboardButton("👩🏻‍💼 基础命令", callback_data="user_command"),
                ],[
                    InlineKeyboardButton("超级管理员命令", callback_data="sudo_command"),
                    InlineKeyboardButton("维护者命令", callback_data="owner_command"),
                ],[
                    InlineKeyboardButton("🔙 返回", callback_data="home_start")
                ],
            ]
        ),
    )


@Client.on_callback_query(filters.regex("user_command"))
@check_blacklist()
async def user_set(_, query: CallbackQuery):
    BOT_NAME = me_bot.first_name
    await query.answer("基础命令")
    await query.edit_message_text(
        f"""✏️ 所有基础命令如下：

» /play (歌曲名称/link) - 播放音乐
» /vplay (video name/link) - 播放视频
» /vstream - 播放直播 yt live/m3u8
» /playlist - 展示播放列表
» /lyric (query) - 从 YouTube 下载歌词
» /video (query) - 从 YouTube 下载视频
» /song (query) - 从 YouTube 下载音乐
» /search (query) - 搜索 YouTube 视频链接
» /ping - pong
» /uptime - 在线状态
» /alive - 播放详情（仅在群组中可用）

⚡️ __Powered by {BOT_NAME} AI__""",
        reply_markup=InlineKeyboardMarkup(
            [[InlineKeyboardButton("🔙 返回", callback_data="command_list")]]
        ),
    )


@Client.on_callback_query(filters.regex("admin_command"))
@check_blacklist()
async def admin_set(_, query: CallbackQuery):
    BOT_NAME = me_bot.first_name
    await query.answer("管理员命令")
    await query.edit_message_text(
        f"""✏️ 所有管理员命令如下：

» /pause - 暂停
» /resume - 回复
» /skip - 跳过
» /stop - 停止
» /vmute - 静音
» /vunmute - 解除静音
» /volume `1-200` - 调整音量（userbot 必须是管理员）
» /reload - 重载管理员信息
» /userbotjoin - 邀请 userbot
» /userbotleave - 让 userbot 退出群组
» /startvc - 开启视频聊天
» /stopvc - 关闭视频聊天

⚡️ __Powered by {BOT_NAME} AI__""",
        reply_markup=InlineKeyboardMarkup(
            [[InlineKeyboardButton("🔙 返回", callback_data="command_list")]]
        ),
    )


@Client.on_callback_query(filters.regex("sudo_command"))
@check_blacklist()
async def sudo_set(_, query: CallbackQuery):
    user_id = query.from_user.id
    BOT_NAME = me_bot.first_name
    if user_id not in SUDO_USERS:
        await query.answer("⚠️ 你没有权限按这个按钮", show_alert=True)
        return
    await query.answer("超级管理员命令")
    await query.edit_message_text(
        f"""✏️ 所有超级管理员命令如下：

» /stats - 获取机器人当前统计数据
» /calls - 显示数据库中所有正在进行的语音聊天群组
» /block (`chat_id`) - 拉黑群组
» /unblock (`chat_id`) - 解除拉黑群组
» /blocklist - 显示拉黑的群组列表
» /speedtest - 测速
» /sysinfo - 显示系统信息
» /logs - 查看当前的机器人日志
» /eval - 运行代码
» /sh - 运行命令

⚡ __Powered by {BOT_NAME} AI__""",
        reply_markup=InlineKeyboardMarkup(
            [[InlineKeyboardButton("🔙 返回", callback_data="command_list")]]
        ),
    )


@Client.on_callback_query(filters.regex("owner_command"))
@check_blacklist()
async def owner_set(_, query: CallbackQuery):
    user_id = query.from_user.id
    BOT_NAME = me_bot.first_name
    if user_id not in OWNER_ID:
        await query.answer("⚠️ 你没有权限按这个按钮", show_alert=True)
        return
    await query.answer("维护者命令")
    await query.edit_message_text(
        f"""✏️ 所有维护者命令如下：

» /gban (`username` or `user_id`) - 全局封禁
» /ungban (`username` or `user_id`) - 全局解封
» /update - 升级
» /restart - 重启
» /leaveall - 退出所有群
» /leavebot (`chat id`) - 退出群组
» /broadcast (`message`) - 群发公告
» /broadcast_pin (`message`) - 群发并且置顶公告

⚡ __Powered by {BOT_NAME} AI__""",
        reply_markup=InlineKeyboardMarkup(
            [[InlineKeyboardButton("🔙 返回", callback_data="command_list")]]
        ),
    )


@Client.on_callback_query(filters.regex("stream_menu_panel"))
@check_blacklist()
async def at_set_markup_menu(_, query: CallbackQuery):
    user_id = query.from_user.id
    a = await _.get_chat_member(query.message.chat.id, query.from_user.id)
    if not a.can_manage_voice_chats:
        return await query.answer("💡 仅拥有 管理语音聊天 权限的管理员可以按按钮", show_alert=True)
    chat_id = query.message.chat.id
    user_id = query.message.from_user.id
    buttons = menu_markup(user_id)
    if chat_id in QUEUE:
        await query.answer("操作面板已开启")
        await query.edit_message_reply_markup(reply_markup=InlineKeyboardMarkup(buttons))
    else:
        await query.answer("❌ 队列为空", show_alert=True)


@Client.on_callback_query(filters.regex("stream_home_panel"))
@check_blacklist()
async def is_set_home_menu(_, query: CallbackQuery):
    a = await _.get_chat_member(query.message.chat.id, query.from_user.id)
    if not a.can_manage_voice_chats:
        return await query.answer("💡 仅拥有 管理语音聊天 权限的管理员可以按按钮", show_alert=True)
    await query.answer("操作面板已关闭")
    user_id = query.message.from_user.id
    buttons = stream_markup(user_id)
    await query.edit_message_reply_markup(reply_markup=InlineKeyboardMarkup(buttons))


@Client.on_callback_query(filters.regex("set_close"))
@check_blacklist()
async def on_close_menu(_, query: CallbackQuery):
    a = await _.get_chat_member(query.message.chat.id, query.from_user.id)
    if not a.can_manage_voice_chats:
        return await query.answer("💡 仅拥有 管理语音聊天 权限的管理员可以按按钮", show_alert=True)
    await query.message.delete()


@Client.on_callback_query(filters.regex("close_panel"))
@check_blacklist()
async def in_close_panel(_, query: CallbackQuery):
    await query.message.delete()
